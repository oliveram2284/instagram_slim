<?php
session_cache_limiter(false);
session_start();

//require 'lib/SmartyView.php';

require 'config.php';
require 'vendor/autoload.php';

require  'vendor/instagram/Instagram.php';
use MetzWeb\Instagram\Instagram;

$instagram = new Instagram(array(
  'apiKey'      => APIKEY,
  'apiSecret'   => APISECRET,
  'apiCallback' => APICALLBACK
));


$app = new \Slim\Slim(array(
    'debug'=>true,
    'templates.path' => 'templates',
));

$app->add(new \Slim\Middleware\SessionCookie(array(
    'expires' => '20 minutes',
    'path' => '/',
    'domain' => null,
    'secure' => false,
    'httponly' => false,
    'name' => 'slim_session',
    'secret' => 'CHANGE_ME',
    'cipher' => MCRYPT_RIJNDAEL_256,
    'cipher_mode' => MCRYPT_MODE_CBC
)));


$app->container->singleton('log', function () {
    $log = new \Monolog\Logger('slim-skeleton');
    $log->pushHandler(new \Monolog\Handler\StreamHandler('logs/app.log', \Monolog\Logger::DEBUG));
    return $log;
});

// Prepare view
$app->view();//new \Slim\Views\Twig());
$app->view->parserOptions = array(
    'charset' => 'utf-8',
    'cache' => realpath('templates/cache'),
    'auto_reload' => true,
    'strict_variables' => false,
    'autoescape' => true
);
$app->view->parserExtensions = array(new \Slim\Views\TwigExtension());





// Define routes
$app->get('/', function () use ($app) {  

    if (!isset($_SESSION['instagram_token']))
    {
        $app->redirect("api/login");  
    }
    
    $app->log->info(" Api index '/' route");
    $app->render('index.php');
});

$app->get('/api/login',function()use($app,$instagram){
    $app->log->info(" Api Login");

    $loginUrl = $instagram->getLoginUrl(array(
      'basic',
      'likes',
      'relationships'
    ));    

    $app->render('login.tpl', array('loginUrl' => $loginUrl));
});

$app->get('/api/successful',function()use($app,$instagram){
    $_SESSION['instagram_code']=$_GET['code'];
   
    $data = $instagram->getOAuthToken($_SESSION['instagram_code']);

    $_SESSION['instagram_token']=$data->access_token;
    $_SESSION['instagram_id']=$data->user->id;

    $app->redirect('/slim/');
});

$app->get('/api/profile',function()use($app,$instagram){
    $profile = $instagram->getUser($_SESSION['instagram_id']);    
    echo json_encode($profile);   
});

$app->get('/api/get_posts',function()use($app,$instagram){
    $posts = $instagram->getUserMedia($_SESSION['instagram_id'],20);    
    echo json_encode($posts);    
});

$app->get('/api/get_followers',function()use($app,$instagram){
    $instagram->setAccessToken( $_SESSION['instagram_token']);
    $followers = $instagram->getUserFollower($_SESSION['instagram_id'],20);    
    echo json_encode($followers);
});

$app->get('/api/get_followings',function()use($app,$instagram){
    $instagram->setAccessToken( $_SESSION['instagram_token']);
    $followings = $instagram->getUserFollows($_SESSION['instagram_id'],20);    
    echo json_encode($followings);
});


$app->get('/api/search/:keywork',function($keywork=null)use($app,$instagram){
    $result = $instagram->getTagMedia($keywork,10);
    echo json_encode($result);   
});




// Run app
$app->run();

