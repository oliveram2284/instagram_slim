<html>
<head>
	<title>Api REST</title>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">

	<!-- Optional theme -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">

	<!-- Latest compiled and minified JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
	<style>
      .login {
        display: block;
        font-size: 20px;
        font-weight: bold;
        margin-top: 50px;
      }
    </style>

</head>
<body>
	
	<div class="container">

		<header class="clearfix">
        <h1>Instagram <span>display your photo stream</span></h1>
      </header>
	  <div class="row">
	  	<div class="col-md-12">
	  		<a class="btn btn-primary" href="<?php echo $loginUrl?>">» Login with Instagram</a>
            <h4>Use your Instagram account to login.</h4>
	  	</div>
	  </div>
	</div>

	
	<script src="/assets/js/bootstrap.js"></script>
</body>
</html>