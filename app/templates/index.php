<html>
<head>
	<title>Api REST</title>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">

	<!-- Optional theme -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">

	<!-- Latest compiled and minified JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>

</head>
<body>
	
	<div class="container">
	  <div class="row">
	  	<div class="col-md-12">
	  		<h1>Instagram</h1>

	  		<div role="tabpanel">  
	  			<!-- Nav tabs -->  
	  			<ul class="nav nav-tabs" role="tablist">    
	  				<li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Profile</a></li>
				    <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Posts</a></li>
				    <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Followers</a></li>
				    <li role="presentation"><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">Following</a></li>
				    <li role="presentation"><a href="#search" aria-controls="search" role="tab" data-toggle="tab">search</a></li>
				  </ul>

				  <!-- Tab panes -->
				  <div class="tab-content">
				    <div role="tabpanel" class="tab-pane active" id="home">
				    	<br>
				    	<div class="row">
				    		<div class="col-xs-8 col-sm-8">
				    		<div class="media">							  
				    			<div class="media-left">							    
				    				<a href="#">							      
				    					<img id="profile_image" class="media-object" src="" alt="...">
				    				</a>							  
				    			</div>							  
				    			<div class="media-body">							    
				    				<h3 class="media-heading" id="profile_username"></h3>
				    				<ul class="list-inline" id="profile_follows">
				    				</ul>						    
				    				<p id="profile_bio">
				    					
								    </p>
								    <a href="" id="profile_website"></a>
							  </div>
							</div>
				    	</div>
				    	</div>
				    </div>
				    <div role="tabpanel" class="tab-pane" id="profile">
				    	<br>
				    	<div class="row" id="div_post"></div>
				    </div>
				    <div role="tabpanel" class="tab-pane" id="messages">
				    	<br>
				    	<div class="row" id="div_followers"></div>
				    </div>
				    <div role="tabpanel" class="tab-pane" id="settings">
				    	<br>
				    	<div class="row" id="div_followings"></div>
				    </div>
				    <div role="tabpanel" class="tab-pane" id="search">
				    	<br>
				    	<div class="row">
				    		<div class="col-xs-12  col-sm-12">
				    			<form class="form-inline" >  
				    				<div class="form-group">    
				    					<label for="search_input">Search</label>    
				    					<input type="text" class="form-control" id="search_input"  name="search_input" placeholder="#dogs">  
				    				</div>   
				    				<button type="submit" class="btn btn-default">Filter</button>
				    			</form>	
				    		</div>
				    	</div>
				    	<div class="row" id="div_result">

				    	</div>
				    </div>
				  </div>

				</div>

	  	</div>
	  </div>
	</div>

	
	<script src="assets/js/bootstrap.js"></script>
	<script src="assets/js/api_client.js"></script>
	<script type="text/javascript">
		$(function(){
			$('#myTab a').click(function (e) {
			  e.preventDefault()
			  $(this).tab('show')
			})

		});
	</script>
</body>
</html>