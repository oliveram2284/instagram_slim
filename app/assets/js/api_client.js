function APIClient(){
	
	var self = this;
	self.startup=function(){
		//console && console.log("==== APIClient.startup");
		self.load_insta_profile();  
		self.load_insta_posts(); 
		self.load_insta_followers(); 
		self.load_insta_followings();
		$("form").submit(self.search_submit);
		
	}
	self.search_submit=function(the_data){
		//console && console.log("==== APIClient.search_submit: %o",the_data);
		//console && console.log("==== APIClient.search_submit: %o",$(this));
		//console && console.log("==== APIClient.search_submit: %o",$(this).serialize());
		var keywork= $("#search_input").val();
		//console && console.log("==== APIClient.search_submit: %o",keywork);
		///api/search
		var ajax_data={};
		ajax_data.url="api/search/"+keywork;
		ajax_data.success=self.search_tag_ajax;
		$.ajax(ajax_data);
		return false;
	}

	self.search_tag_ajax=function(the_data){
		//console && console.log("search_tag_ajax: %o",the_data);	
		var the_json=JSON.parse(the_data);
		console && console.log("search_tag_ajax the_data = %o",the_json);

	}
	self.load_insta_profile=function(){
		//console && console.log("==== APIClient.load_insta_profile");
		var ajax_data={};
		ajax_data.url="api/profile";
		ajax_data.success=self.get_profile_ajax;
		$.ajax(ajax_data);
	}

	self.load_insta_posts=function(){
		//console && console.log("==== APIClient.load_insta_posts");
		var ajax_data={};
		ajax_data.url="api/get_posts";
		ajax_data.success=self.get_posts_ajax;
		$.ajax(ajax_data);
	}

	self.load_insta_followers=function(){
		//console && console.log("==== APIClient.load_insta_followers");
		var ajax_data={};
		ajax_data.url="api/get_followers";
		ajax_data.success=self.get_followers_ajax;
		$.ajax(ajax_data);
	}


	self.load_insta_followings=function(){
		//console && console.log("==== APIClient.load_insta_followings");
		var ajax_data={};
		ajax_data.url="api/get_followings";
		ajax_data.success=self.get_followings_ajax;
		$.ajax(ajax_data);
	}

	self.get_profile_ajax=function(the_data){
		//console && console.log("get_profile_ajax the_data = %o");	
		var the_json=JSON.parse(the_data);

		$("#profile_image").attr('src',the_json.data.profile_picture);
		$("#profile_username").text(the_json.data.username);
		$("#profile_bio").text(the_json.data.bio);
		$("#profile_follows").append('<li class=""><label> Posts       <span class="label label-primary">'+the_json.data.counts.media+'</span>  </label>    </li>');
		$("#profile_follows").append('<li class=""><label> Followers       <span class="label label-primary">'+the_json.data.counts.followed_by+'</span>  </label>    </li>');
		$("#profile_follows").append('<li class=""><label> Following       <span class="label label-primary">'+the_json.data.counts.follows+'</span>  </label>    </li>');
	};

	self.get_posts_ajax=function(the_data){
		//console && console.log("get_posts_ajax the_data = %o");
		var the_json=JSON.parse(the_data);	

		$.each(the_json.data,function(index,row){
			
			var description="";
			if(row.caption!== null){
				description=row.caption.text;
			}
			var output= '<div class="col-md-3">'+ 			    				
										'<div class="thumbnail"> '+     
				    					'	<img src="'+row.images.low_resolution.url+'" alt="...">      '+
				    					'	<div class="caption">'+  
			    							'	<p>'+description+'</p>'+		    					
			    						' </div>'+			    				
			    					'</div>  '+			    			
			    				'</div>';
			$("#div_post").append(output);
		});
	}

	self.get_followers_ajax=function(the_data){
		//console && console.log("get_followers_ajax the_data = %o");
		var the_json=JSON.parse(the_data);			

		$.each(the_json.data,function(index,row){
			var output= '<div class="col-md-3">'+ 			    				
										'<div class="thumbnail"> '+     
				    					'	<img src="'+row.profile_picture+'" alt="...">      '+
				    					'	<div class="caption">'+  
				    						'<h3>'+row.full_name+'</h3>'+
			    							'	<p>'+row.bio+'</p>'+		    					
			    						' </div>'+			    				
			    					'</div>  '+			    			
			    				'</div>';		
			$("#div_followers").append(output);

		});
	}

	self.get_followings_ajax=function(the_data){
		//console && console.log("get_followings_ajax the_data = %o");
		var the_json=JSON.parse(the_data);	
		$.each(the_json.data,function(index,row){
			var output= '<div class="col-md-3">'+ 			    				
										'<div class="thumbnail"> '+     
				    					'	<img src="'+row.profile_picture+'" alt="...">      '+
				    					'	<div class="caption">'+  
				    						'<h3>'+row.full_name+'</h3>'+
			    							'	<p>'+row.bio+'</p>'+		    					
			    						' </div>'+			    				
			    					'</div>  '+			    			
			    				'</div>';  				
			$("#div_followings").append(output);
		});
	}

	self.search_tag_ajax=function(the_data){
		//console && console.log("search_tag_ajax: %o");//,the_data);	
		var the_json=JSON.parse(the_data);
		//console && console.log("search_tag_ajax the_data = %o",the_json);
		$("#div_result").empty();
		$.each(the_json.data,function(index,row){
			//console && console.log("search_tag_ajax index : %o",index);	
			//console && console.log("search_tag_ajax index : %o",row);	
			var description="";
			if(row.caption!== null){
				description=row.caption.text;
			}
			var output= '<div class="col-md-3">'+ 			    				
										'<div class="thumbnail"> '+     
				    					'	<img src="'+row.images.low_resolution.url+'" alt="...">      '+
				    					'	<div class="caption">'+  
			    							'	<p>'+description+'</p>'+		    					
			    						' </div>'+			    				
			    					'</div>  '+			    			
			    				'</div>';		
			$("#div_result").append(output);
		});
	}
}
var _api_client = new APIClient();
$(_api_client.startup);